#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

#source /opt/hpc/build/dyn_procs_setup/load.sh all >> /dev/null
export ASAN_OPTIONS=detect_leaks=0:detect_odr_violation=0:verbosity=3
export PMIX_DEBUG=10
#export PMIX_MCA_pmix_client_event_verbose=100
#export PMIX_MCA_ptl_base_verbose=10
#export PMIX_MCA_gds=hash

export PMIX_MCA_gds=hash

echo "run libpfasst"
$SCRIPT_DIR/../build/libpfasst_example.exe $SCRIPT_DIR/probin.nml >> ./libpfasst_output.txt 2>&1
exit 0
