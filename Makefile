BASE_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# make arguments: Specify paths to dependencies
MPI ?= 
LIBPFASST ?=
HYPRE ?=
INSTALL_DIR ?= $(BASE_DIR)build
DEBUG ?= 0

CC = gcc
MY_CFLAGS = $(CFLAGS) -Wall -Werror -Wextra -pedantic -fPIC

LTARGET=
HTARGET=
# Set default paths for dependencies
# Default MPI location in docker cluster
ifndef MPI
	MPI=/opt/hpc/install/ompi
endif

# Default to internal version
ifndef LIBPFASST
	LIBPFASST=$(BASE_DIR)/libpfasst
	LTARGET=libpfasst/install/lib/libpfasst.a
endif

# Default to internal version
ifndef HYPRE
	HYPRE=$(BASE_DIR)/hypre/install
	HTARGET=hypre/install/lib/libHYPRE.a 
endif

HYPRE_CONFIG=
ifeq ($(DEBUG), 1)
	HYPRE_CONFIG=--with-extra-CFLAGS=-g --with-extra-CXXFLAGS=-g
else ifeq ($(DEBUG), 2)
	HYPRE_CONFIG=--with-extra-CFLAGS=-fsanitize=address -g --with-extra-CXXFLAGS=-fsanitize=address -g
endif

all: libpfasst_app job_mix

libpfasst_app: $(LTARGET) $(HTARGET)
	mkdir -p $(INSTALL_DIR)
	cd app && make -j all install DEBUG=$(DEBUG) LIBPFASST=$(LIBPFASST) HYPRE=$(HYPRE) INSTALL_DIR=$(INSTALL_DIR)

libpfasst/install/lib/libpfasst.a:
	cd libpfasst && make -j DEBUG=$(DEBUG) && make

hypre/install/lib/libHYPRE.a: 
	cd hypre/src && ./configure $(HYPRE_CONFIG) --prefix=$(BASE_DIR)/hypre/install --disable-fortran && make -j all 

job_mix:
	echo "1,$(BASE_DIR)submissions/libpfasst_bs.batch," > $(BASE_DIR)/submissions/mix1.mix
	echo "10,$(BASE_DIR)submissions/libpfasst_gs.batch," >> $(BASE_DIR)/submissions/mix1.mix
	echo "11,$(BASE_DIR)submissions/libpfasst_gs.batch," >> $(BASE_DIR)/submissions/mix1.mix
	echo "12,$(BASE_DIR)submissions/libpfasst_bs.batch," >> $(BASE_DIR)/submissions/mix1.mix
	echo "13,$(BASE_DIR)submissions/libpfasst_bs.batch, {'terminate_soon': True}" >> $(BASE_DIR)/submissions/mix1.mix

clean: 
	rm -rf build/*
	rm -f submissions/mix1.mix
	cd app && make clean
ifndef LIBPFASST
	cd libpfasst && make clean
endif
ifndef HYPRE
	cd hypre/src && make clean
endif
