# Example: How to use LibPFASST Examples
This directory provides a concrete example of using LibPFASST with DynRM

## Directory Structure:
* run_test_dynrm.py: Top level script to run dynrm example
* build: build directory for example application
* output: output diectory to write output to 
* app: Directory containing the source files for the example application 
* submissions: directory containing submission files
    * The provided .batch and .mix files work with the DefaultTaskGraphCreationModule
* topology_file: directory containing topology description files
    * The provided .yaml file works with the DefaultTopologyGraphCreationModule
    * The specified host names refer to the docker cluster environment

## Requirements:
* DynRM 
* PRRTE
* OpenPMIx
* Open-MPI
* DynLibPFASST (if not using internal version)
* Hypre (if not using internal version)
* asyncio (included in docker image)
* pyyaml (included in docker image)

Requirements can be setup in the docker cluster using the dyn_procs_setup repo:

```
cd /opt/hpc/build/dyn_procs_setup
```
Install libpfasst_examples and required packages:
```
./envs.sh libpfasst_examples
```

Load packages into environment (required to run examples manually):
```
source envs.sh libpfasst_examples
```

## How to run the examples 
There are some predefined tests in the dyn_procs_setup repo:
```
./tests libpfasst_examples
```

## How to run the examples manually

The basic command to run the example is:

```
python3 run_test_dynrm.py 
    --topology_file=[path to file (relative to topology_files directory)] 
    --submission_file=[path to file (relative to submissions directory)] 
    --verbosity={0, ..., 10} // Recommended: 1 or 9, (11+ for debugging)
    --output_dir=[name of the directory where output should be written to (relative to output dir)] 
    --step_size={'single', 'linear', 'power_of_2'} //Step size for steepest ascend. Recommended: 'single'
```

To execute the expansion test case run:

```
python3 run_test_dynrm.py \
    --topology_file=8_node_system.yaml \
    --submission_file=libpfasst_expand.batch \
    --verbosity=9 \
    --output_dir=expansion_test \
    --step_size=single 
```

To execute the job mix test case run:

```
python3 run_test_dynrm.py \
    --topology_file=8_node_system.yaml \
    --submission_file=mix1.mix \
    --verbosity=9 \
    --output_dir=job_mix_test \ 
    --step_size=linear
```